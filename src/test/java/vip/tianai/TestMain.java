package vip.tianai;

import org.springframework.util.StopWatch;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class TestMain {

    public static void main(String[] args) throws InterruptedException {


        ExpiringMap<String, Integer> expiringMap = new ConCurrentExpiringMap<>();
        expiringMap.init();


        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Random random = new Random();
        for (int i = 0; i < 1000000; i++) {

            expiringMap.put("KEY" + i, i, (long) random.nextInt(10000), TimeUnit.MILLISECONDS);
        }
        stopWatch.stop();
        System.out.println("耗时:" + stopWatch.getTotalTimeMillis());

        TimeUnit.SECONDS.sleep(5);

        for (int i = 0; i < 100000; i++) {
            String key = "KEY" + new Random().nextInt(1000000);
            Integer data = expiringMap.get(key);
            System.out.println("读取数据 key=" + key + "value=" + data);
        }

        TimeUnit.DAYS.sleep(1);
    }

}
